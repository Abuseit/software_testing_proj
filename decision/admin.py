from django.contrib import admin
from .models import Activity, Choice

#прикрутить регистрацию типов тут
class ChoiceInline(admin.TabularInline):
  model = Choice
  extra = 3

class ActivityAdmin(admin.ModelAdmin):
  list_display= ('context', 'creator', 'email', 'id')
  inlines = [ChoiceInline]

admin.site.register(Activity, ActivityAdmin)