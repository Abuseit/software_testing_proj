from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse
from django.utils import timezone
from django.urls import reverse
from django.core.mail import send_mail
import json
import datetime

from .models import Activity

def index(request):
  return render(request, 'decision/index.html')

def new(request):
  return render(request, 'decision/new.html')

def create(request):
  context = request.POST['context']
  creator = request.POST['creator']
  email = request.POST['email']
  description = request.POST['description']
  options_list = request.POST.getlist('option')
  expire_on = timezone.now() + datetime.timedelta(days=7)

  new_activity = Activity(context=context, creator=creator, email=email, description=description, expire_on=expire_on)
  new_activity.save()

  for i in range(len(options_list)):
    option = options_list[i]
    new_activity.choice_set.create(name=option)

  message = {
    'title': 'Cіз сауалнама құрдыңыз',
    'content': 'Құпия сілтеме сізге электрондық пошта арқылы жіберілді. Сіз білуі керек адамдарға сілтеме жібере аласыз. Дауыс беру сессиясы аяқталған күні жабық болады'
  }

#пока не работает :(
  send_mail(
      'Сауалнама',
      f'Сіз құрған сауалнама {new_activity.context}, сіздің админ токеніңіз {new_activity.admin_token}, сіздің сауалнамаға сілтеме: localhost:8000/show/{new_activity.id} ',
      'md@md.com',
      [email,],
      fail_silently=True,
  )

  return render(request, 'decision/info.html', {'message': message})

def show(request, activity_id):
  activity = Activity.objects.get(pk=activity_id)
  choice_list = activity.choice_set.all()
  return render(request, 'decision/show.html', {'activity': activity, 'choice_list': choice_list})

def vote(request, activity_id):
  activity = Activity.objects.get(pk=activity_id)
  choice_list = activity.choice_set.all()
  try:
    selected_choice = activity.choice_set.get(name=request.POST['choice'])
  except (KeyError, Choice.DoesNotExist):
    return render(request, 'decision/show.html', {'activity': activity, 'choice_list': choice_list, 'error_message': 'You did not select anything'})
  else:
    selected_choice.votes_count += 1
    selected_choice.save()
    return HttpResponseRedirect(reverse('decision:success', args=(activity.id,)))

def success(request, activity_id):
  activity = Activity.objects.get(pk=activity_id)
  choice_list = list(activity.choice_set.all().values('name', 'votes_count'))

  success_message = f'Дауыс бергеніңіз үшін рахмет. Соңғы нәтижелер {activity.expire_on} күні шығарылады'
  return render(request, 'decision/success.html', {'choice_list': choice_list, 'activity': activity, 'message': success_message})

def admin(request, admin_token):
  activity = get_object_or_404(Activity, admin_token=admin_token)
  choice_list = list(activity.choice_set.all().values('name', 'votes_count'))
  if request.GET['email'] == activity.email:
    return render(request, 'decision/admin.html', {'activity': activity, 'choice_list': choice_list})
  else:
    return HttpResponse('Denied! You do not have access')

def update(request, activity_id):
  activity = get_object_or_404(Activity, pk=activity_id)

  old_choice_list = activity.choice_set.all()
  new_choice_list = request.POST.getlist('option')

  for old_choice in old_choice_list:
    try:
      new_choice_list.index(old_choice.name)
    except:
      old_choice.delete()

  for new_choice in new_choice_list:
    try:
      choice = activity.choice_set.get(name=new_choice)
    except:
      if new_choice != '':
        activity.choice_set.create(name=new_choice)

  
  choice_list = activity.choice_set.all()
  
  success_message = 'Сіз сауалнамаңызды жаңартыңыз'

  send_mail(
      'Сауалнама',
      'Сіздің сауалнамаңыз жаңартылды!',
      'md@md.com',
      [email,],
      fail_silently=True,
  )
  return render(request, 'decision/success.html', {'activity': activity, 'choice_list': choice_list, 'message': success_message})


def delete(request, activity_id):
  activity = get_object_or_404(Activity, pk=activity_id)
  activity_context = activity.context
  activity.delete()

  send_mail(
      'Сауалнама',
      f'Сіздің \'{activity_context}\' сауалнамаңыз жойылды. Соңымен бірге дауыстар да жойылды',
      'md@md.com',
      [email,],
      fail_silently=True,
  )

  message = {
    'title': 'Cіз сауалнамаңызды жойдыңыз',
    'content': 'Сіз сауалнамаңызды жойдыңыз, дауыстар жойылды'
  }
  return render(request, 'decision/info.html', {'message': message})

